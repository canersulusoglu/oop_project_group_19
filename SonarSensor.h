/**
 * @file SonarSensor.h
 * @Author Caner Sulusoglu (cscaner26@gmail.com)
 * @date January, 2020
 * @brief Provides data capture and management for the sonar distance sensor.
 *
 * Provides data capture and management for the sonar distance sensor.
 */
#ifndef SONARSENSOR_H
#define SONARSENSOR_H
#include "RangeSensor.h"

//! SonarSensor Class
/*!
 Provides data capture and management for the sonar distance sensor.
*/
class SonarSensor : public RangeSensor{
public:
	//! A constructor.
	SonarSensor(PioneerRobotAPI* robot) :RangeSensor(robot) { this->ranges = new float[16]; };
	//! A destructor.
	~SonarSensor();
	//! Method that returns range of sensor by their index.
	float getRange(int index);
	//! Method that returns angle of sensor by their index.
	float getAngle(int index);
	//! Method that returns sensor that has maximum range and store its index to parameter.
	float getMax(int& index);
	//! Method that returns sensor that has minimum range and store its index to parameter.
	float getMin(int& index);
	//! Method that returns the range of the smallest distance between startAngle and endAngle angles and store its angle to parameter.
	float getClosestRange(float startAngle, float endAngle, float& angle);
	//! Method that store all range of sensors to ranges 16 capacity array.
	void updateSensor(float ranges[16]);
};
#endif
