/**
 * @file RobotInterface.h
 * @Author Metin Cem Demirdas (cem.dmrds@gmail.com)
 * @date January, 2021
 * @brief an inherited class of RobotInterface, which is controlled by RobotControl class .
 *
 * @brief an inherited class .
 */
#ifndef PIONEERROBOTINTERFACE_H
#define PIONEERROBOTINTERFACE_H
#include"RobotInterface.h"
#include"PioneerRobotAPI.h"
#include"RangeSensor.h"
#include"LaserSensor.h"
#include"SonarSensor.h"
class PioneerRobotInterface : public RobotInterface {
private:
	PioneerRobotAPI* RobotAPI;
public:
	//! Constructor of RobotInterface class. checks if the robot is connected. if it's connected it adds the sensors.
	PioneerRobotInterface() {
		RobotAPI = new PioneerRobotAPI();
		if (!RobotAPI->connect()) {
			throw std::exception("Robot couldn't connect.");
			isRobotConnected = false;
		}
		else {
			isRobotConnected = true;
			RangeSensor* sonarSensor = new SonarSensor(RobotAPI);
			RangeSensor* laserSensor = new LaserSensor(RobotAPI);
			rangeSensors->push_back(sonarSensor);
			rangeSensors->push_back(laserSensor);
		}
	};
	//! turns robot to the left, then fixes the position of the robot .
	void turnLeft();
	//! turns robot to the right, then fixes the position of the robot .
	void turnRight();
	//! moves the robot forwards, then fixes the position of the robot .
    void forward(float speed);
	//! prints current position of the robot.
	void print() const;
	//! moves the robot backwards, then fixes the position of the robot .
	void backward(float speed);
	//! stops turning the robot, then fixes the position of the robot .
	void stopTurn();
	//! stops moving the robot, then fixes the position of the robot .
	void stopMove();
	//!Gets the position of the robot.
	Pose getPose() const;
	//!Gets the position of the robot.
	void setPose(Pose* pose);
	//! updates each sensor
	void updateSensors();
	//!  Gets all the sensors.
	list<RangeSensor*>* getRangeSensors() const;
};
#endif;