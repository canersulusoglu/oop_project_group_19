#include "Record.h"

Record::Record()
{
	this->fileName = ""; 
}

Record::Record(string file_name)
{
	this->fileName = file_name;
	file.open(this->fileName, ios::app);
}
/*!
	\Opens file.
*/
bool Record::openFile()
{
	if (!this->file.is_open()) { 
		this->file.open(this->fileName, ios::app);
	}
	if (file.is_open()) {
		return true;
	}

	return false;
}
/*!
	\Closes file.
*/
bool Record::closeFile()
{
	if (this->file.is_open()) {
		this->file.close();
	}
	if (!this->file.is_open()) {
		return true;
	}

	return false;
}
/*!
	\Writes a line of data to the file.
*/
bool Record::writeLine(string str)
{
	if (this->file.is_open()) 
	{
		this->file.seekg(0, ios::end);
		this->file << str << endl;
		if (file.good())
			return true;
	}
	
	return false;
}

void Record::setFileName(string name)
{
	this->fileName = name;
}
/*!
	\Reads a line of data from file.
*/
string Record::readLine()
{
	string line;
	this->file >> line;
	return line;
}
/*!
	\Writes datas to the file.
*/
ostream& operator<<(ostream& os, const Record&)
{

	return os;
}
/*!
	\Claims datas from the file.
*/
istream& operator>>(istream& os, Record&)
{
	return os;
}