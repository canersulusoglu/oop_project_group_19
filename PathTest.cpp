/**
 * @file Pose.h
 * @Author Tarik Kemal Gundogdu (tarikkemal314@gmail.com)
 * @date January, 2020
 * @brief Path class test file.
 *
 * Path class test file.

 */

#include "Path.h"
#include <iostream>

using namespace std;

void test1(Path* path);
void test2(Path* path);
void test3(Path* path);
void test4(Path* path);
void test5(Path* path);

int main()
{
	Path* path = new Path();

	try
	{
		test1(path);
		test2(path);
		test3(path);
		test4(path);
		test5(path);
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << endl;
	}

	delete path;
}


void test1(Path* path)
{
	cout << "---Add Pose Test---" << endl << endl; // delete poses
	Pose* pose  = new Pose();
	Pose* pose2 = new Pose(2, 3, 4);
	path->addPos(*pose);
	path->addPos(*pose2);
	cout << *(path);
	cout << "-----------------------------" << endl << endl;
	delete pose;
	delete pose2;
}

void test2(Path* path)
{
	cout << "---Insert pose test---" << endl << endl;
	Pose* pose3 = new Pose(5, 6, 7);
	Pose* pose4 = new Pose(8, 9, 10);
	path->insertPos(0, *pose3);
	path->insertPos(1, *pose4);
	cout << *(path);
	cout << "-----------------------------" << endl << endl;
	delete pose3;
	delete pose4;
}

void test3(Path* path)
{
	cout << "---Get pose test---" << endl << endl;
	path->getPos(3);
	path->getPos(6);
	cout << "-----------------------------" << endl << endl;

}

void test4(Path* path)
{
	cout << "--->> operator test---" << endl << endl;
	Pose* pose5 = new Pose(11, 12, 13);
	*(path) >> *(pose5);
	cout << *(path);
	cout << "-----------------------------" << endl << endl;
	delete pose5;
}

void test5(Path* path)
{
	cout << "---Remove pose test---" << endl << endl;
	path->removePos(2);
	path->removePos(1);
	cout << *(path);
	cout << "-----------------------------" << endl << endl;
}

