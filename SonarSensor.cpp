#include "SonarSensor.h"

SonarSensor::~SonarSensor() {
	delete ranges;
}

/*!
 \param index an integer argument.
 \return The range of sensor by index.
*/
float SonarSensor::getRange(int index) {
	if (index >= 0 && index <= 15){
		return ranges[index];
	}
	else {
		throw std::out_of_range("Out of range.");
	}
}

/*!
 \param index an integer argument.
 \return The angle of sensor by index.
*/
float SonarSensor::getAngle(int index) {
	float robotAngle = robotAPI->getTh(); // For Relative Angle. Without that it will be absolute angle.
	if (index == 0 || index == 15) {
		return -90 + robotAngle;
	}
	else if (index == 1 || index == 14) {
		return -50 + robotAngle;
	}
	else if (index == 2 || index == 13) {
		return -30 + robotAngle;
	}
	else if (index == 3 || index == 12) {
		return -10 + robotAngle;
	}
	else if (index == 4 || index == 11) {
		return 10 + robotAngle;
	}
	else if (index == 5 || index == 10) {
		return 30 + robotAngle;
	}
	else if (index == 6 || index == 9) {
		return 50 + robotAngle;
	}
	else if (index == 7 || index == 8) {
		return 90 + robotAngle;
	}
	else {
		throw std::invalid_argument("Out of range.");
	}
}

/*!
 \param index an reference integer argument.
 \return The maximum range.
*/
float SonarSensor::getMax(int& index) {
	int MaxIndex = 0;
	float Max = ranges[MaxIndex];
	for (int i = 1; i < 16; i++){
		if (ranges[i] > Max) {
			Max = ranges[i];
			MaxIndex = i;
		}
	}
	index = MaxIndex;
	return Max;
}

/*!
 \param index an reference integer argument.
 \return The minimum range.
*/
float SonarSensor::getMin(int& index) {
	int MinIndex = 0;
	float Min = ranges[MinIndex];
	for (int i = 1; i < 16; i++) {
		if (ranges[i] < Min) {
			Min = ranges[i];
			MinIndex = i;
		}
	}
	index = MinIndex;
	return Min;
}

/*!
 \param startAngle an float argument.
 \param endAngle an float argument.
 \param angle an reference float argument.
 \return The range of the smallest distance between startAngle and endAngle angles.
*/
float SonarSensor::getClosestRange(float startAngle, float endAngle, float& angle) {
	float ClosestRange = ranges[0];
	float ClosestAngle = getAngle(0);

	float laserAngle;
	for (int i = 1; i < 16; i++) {
		laserAngle = getAngle(i);
		if (startAngle < laserAngle && endAngle > laserAngle) {
			if (ranges[i] < ClosestRange) {
				ClosestAngle = laserAngle;
				ClosestRange = ranges[i];
			}
		}
	}
	angle = ClosestAngle;
	return ClosestRange;
}

/*!
 \param ranges[16] an float array argument.
*/
void SonarSensor::updateSensor(float ranges[16]) {
	robotAPI->getSonarRange(this->ranges);
	if(ranges != NULL)
		ranges = this->ranges;
}