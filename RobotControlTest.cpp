/**
 * @file RobotControlTest.cpp
 * @Author Caner Sulusoglu (cscaner26@gmail.com)
 * @date January, 2020
 * @brief RobotControl class test file.
 *
 * RobotControl class test file.
 */

#include <iostream>
#include "RobotControl.h"
using namespace std;

PioneerRobotAPI* robot;
RobotControl* robotControl;

void Test1();
void Test2();
void Test3();
void Test4();
void Test5();
void Test6();
void Test7();
void Test8();
void Test9();

int main() {
	robot = new PioneerRobotAPI();
	if (!robot->connect()) {
		cout << "Could not connect..." << endl;
		return 0;
	}
	try
	{
		robotControl = new RobotControl(robot);

		Test1();
		Test2();
		Test3();
		Test4();
		Test5();
		Test6();
		Test7();
		Test8();
		Test9();
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << endl;
	}

	robot->disconnect();
	delete robotControl;
	delete robot;
	return 0;
}

void Test1() {
	cout << "----  Moving Forward Test   ----" << endl;
	robotControl->print();
	robotControl->forward(1000);
	robotControl->print();
	robotControl->forward(1000);
	robotControl->print();
	robotControl->forward(1000);
	robotControl->print();
	robotControl->stopMove();
	cout << "--------------------------------" << endl;
}

void Test2() {
	cout << "----  Moving Backward Test  ----" << endl;
	robotControl->print();
	robotControl->backward(1000);
	robotControl->print();
	robotControl->backward(1000);
	robotControl->print();
	robotControl->backward(1000);
	robotControl->print();
	robotControl->stopMove();
	cout << "--------------------------------" << endl;
}

void Test3() {
	cout << "-----  Turning Right Test  -----" << endl;
	robotControl->print();
	robotControl->turnRight();
	robotControl->print();
	robotControl->turnRight();
	robotControl->print();
	robotControl->turnRight();
	robotControl->print();
	robotControl->stopTurn();
	cout << "--------------------------------" << endl;
}

void Test4() {
	cout << "-----  Turning Left Test  -----" << endl;
	robotControl->print();
	robotControl->turnLeft();
	robotControl->print();
	robotControl->turnLeft();
	robotControl->print();
	robotControl->turnLeft();
	robotControl->print();
	robotControl->stopTurn();
	cout << "--------------------------------" << endl;
}

void Test5() {
	cout << "-----  Forward + Turn Right Test  -----" << endl;
	robotControl->print();
	robotControl->forward(1000);
	robotControl->turnRight();
	robotControl->print();
	robotControl->forward(1000);
	robotControl->turnRight();
	robotControl->print();
	robotControl->forward(1000);
	robotControl->turnRight();
	robotControl->print();
	robotControl->stopTurn();
	robotControl->stopMove();
	cout << "---------------------------------------" << endl;
}

void Test6() {
	cout << "-----  Forward + Turn Left Test  -----" << endl;
	robotControl->print();
	robotControl->forward(1000);
	robotControl->turnLeft();
	robotControl->print();
	robotControl->forward(1000);
	robotControl->turnLeft();
	robotControl->print();
	robotControl->forward(1000);
	robotControl->turnLeft();
	robotControl->print();
	robotControl->stopTurn();
	robotControl->stopMove();
	cout << "---------------------------------------" << endl;
}

void Test7() {
	cout << "-----  Backward + Turn Right Test  -----" << endl;
	robotControl->print();
	robotControl->backward(1000);
	robotControl->turnRight();
	robotControl->print();
	robotControl->backward(1000);
	robotControl->turnRight();
	robotControl->print();
	robotControl->backward(1000);
	robotControl->turnRight();
	robotControl->print();
	robotControl->stopTurn();
	robotControl->stopMove();
	cout << "---------------------------------------" << endl;
}

void Test8() {
	cout << "-----  Backward + Turn Left Test  -----" << endl;
	robotControl->print();
	robotControl->backward(1000);
	robotControl->turnLeft();
	robotControl->print();
	robotControl->backward(1000);
	robotControl->turnLeft();
	robotControl->print();
	robotControl->backward(1000);
	robotControl->turnLeft();
	robotControl->print();
	robotControl->stopTurn();
	robotControl->stopMove();
	cout << "---------------------------------------" << endl;
}

void Test9() {
	cout << "-----  Get Pose Test  -----" << endl;
	Pose* pose1 = robotControl->getPose();
	cout << "Pose 1: (" << pose1->getX() << "," << pose1->getY() << "," << pose1->getTh() << ")" << endl;
	cout << "Forward; " << endl;
	robotControl->forward(1000);
	robotControl->stopMove();
	Pose* pose2 = robotControl->getPose();
	cout << "Pose 2: (" << pose2->getX() << "," << pose2->getY() << "," << pose2->getTh() << ")" << endl;
	cout << "Backward; " << endl;
	robotControl->backward(1000);
	robotControl->stopMove();
	Pose* pose3 = robotControl->getPose();
	cout << "Pose 3: (" << pose3->getX() << "," << pose3->getY() << "," << pose3->getTh() << ")" << endl;
	cout << "Turn Right + Forward; " << endl;
	robotControl->turnRight();
	robotControl->forward(1000);
	robotControl->stopTurn();
	robotControl->stopMove();
	Pose* pose4 = robotControl->getPose();
	cout << "Pose 4: (" << pose4->getX() << "," << pose4->getY() << "," << pose4->getTh() << ")" << endl;
	cout << "Turn Left + Backward; " << endl;
	robotControl->turnLeft();
	robotControl->backward(1000);
	robotControl->stopTurn();
	robotControl->stopMove();
	Pose* pose5 = robotControl->getPose();
	cout << "Pose 5: (" << pose5->getX() << "," << pose5->getY() << "," << pose5->getTh() << ")" << endl;
	cout << "---------------------------------------" << endl;
}