#include"PioneerRobotInterface.h"

void PioneerRobotInterface::turnLeft() {
	RobotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
	Sleep(1000);
	setPose(new Pose(RobotAPI->getX(), RobotAPI->getY(), RobotAPI->getTh()));
}
void PioneerRobotInterface::turnRight() {
	RobotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
	Sleep(1000);
	setPose(new Pose(RobotAPI->getX(), RobotAPI->getY(), RobotAPI->getTh()));
}
/*!
 \param speed a float argument.
*/
void PioneerRobotInterface::forward(float speed) {
	RobotAPI->moveRobot(speed);
	Sleep(1000);
	setPose(new Pose(RobotAPI->getX(), RobotAPI->getY(), RobotAPI->getTh()));
}
/*!
 \param speed a float argument.
*/
void PioneerRobotInterface::backward(float speed) {
	RobotAPI->moveRobot(-1*speed);
	Sleep(1000);
	setPose(new Pose(RobotAPI->getX(), RobotAPI->getY(), RobotAPI->getTh()));
}
void PioneerRobotInterface::print() const {
	printf("position: ");
	printf("(%f,%f,%f)\n", RobotAPI->getX(), RobotAPI->getY(), RobotAPI->getTh());
}
void PioneerRobotInterface::stopTurn() {
	RobotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
	Sleep(1000);
	setPose(new Pose(RobotAPI->getX(), RobotAPI->getY(), RobotAPI->getTh()));
}
void PioneerRobotInterface::stopMove() {
	RobotAPI->stopRobot();
	Sleep(1000);
	setPose(new Pose(RobotAPI->getX(), RobotAPI->getY(), RobotAPI->getTh()));
}
/*!
 \return pose of the robot
*/
Pose PioneerRobotInterface::getPose() const {
	Pose pose;
	pose.setX(RobotAPI->getX());
	pose.setY(RobotAPI->getY());
	pose.setTh(RobotAPI->getTh());
	return pose;
}
/*!
 \param pose a Pose* argument.
*/
void PioneerRobotInterface::setPose(Pose* pose) {
	RobotAPI->setPose(pose->getX(),pose->getY(),pose->getTh());
}
void PioneerRobotInterface::updateSensors() {
	for (auto it = rangeSensors->begin(); it != rangeSensors->end(); ++it) {
		(*it)->updateSensor(NULL);
	}
}
/*!
 \return all range sensors.
*/
list<RangeSensor*>* PioneerRobotInterface::getRangeSensors() const {
	return this->rangeSensors;
}