#include "RangeSensor.h"

RangeSensor::RangeSensor(PioneerRobotAPI* robot)
{
	this->robotAPI = robot;
}

RangeSensor::~RangeSensor()
{
	if (ranges == NULL) {
		delete this->ranges;
	}
}
