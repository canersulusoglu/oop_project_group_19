/**
 * @file Node.h
 * @Author Tarik Kemal Gundogdu (tarikkemal314@gmail.com)
 * @date January, 2020
 * @brief Hold the coordinates.
 *
 * Hold the coordinates and the next node.

 */
#ifndef NODE_H
#define NODE_H
#include "Pose.h"

 //! Node Class
 /*!
 Hold the coordinates and the next node. Used in Path class.
 */
class Node {

public:
	Node* next;
	Pose pose;
	//! A constructor.
	Node();
};

#endif

