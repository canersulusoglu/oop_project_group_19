/**
 * @file EncryptionTest.cpp
 * @Author Metin Cem Demirdas (cem.dmrds@gmail.com)
 * @date January, 2021
 * @brief Encryption class test code.
 *
 * Encryption class test code.
 */
#include<iostream>
using namespace std;
#include"Encryption.h"
void test1();
void test2();
void test3();
void test4();
void test5();

void main()
{

	try
	{
		test1();
		test2();
		test3();
		test4();
		test5();
	}
	catch (const std::exception & e)
	{
		cerr << e.what() << endl;
	}
}
void test1() {
	Encryption a;
	int x;
	cout << "number to be encrypted: " << 1000 << endl;
	a.encrypt(1000);
}
void test2() {
	Encryption a;
	int x;
	cout << "number to be encrypted: " << 2490 << endl;
	a.encrypt(2490);
}
void test3() {
	Encryption a;
	int x;
	cout << "number to be decrypted: " << 6791 << endl;
	x = a.decrypt(6791);
}
void test4() {
	Encryption a;
	int x;
	cout << "number to be encrypted: " << 7787 << endl;
	x = a.decrypt(7787);
}
void test5() {
	Encryption a;
	int x;
	cout << "number to be encrypted: " << 999 << endl;
	x = a.encrypt(999);
}