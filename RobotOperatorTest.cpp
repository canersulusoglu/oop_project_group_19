/**
 * @file RobotOperatorTest.cpp
 * @Author Metin Cem Demirdas (cem.dmrds@gmail.com)
 * @date January, 2021
 * @brief RobotOperator class test code.
 *
 * RobotOperator class test code.
 */
#include<iostream>
#include<string>
using namespace std;
#include"RobotOperator.h"
void test1();
void test2();
void test3();
void test4();

void main()
{
	
	try
	{
		test1();
		test2();
		test3();
	}
	catch (const std::exception & e)
	{
		cerr << e.what() << endl;
	}
}
void test1() {
	int x;
	RobotOperator q("name", "surname", 1000);
	cout << "-----------checkAccessCodeTest-------------- " << endl;
	x = q.checkAccessCode(1000);
	cout << "checkAccessCodeTest test was successful."<< x << endl;
	cout << "---------------------------------------------" << endl;
}
void test2() {
	int x;
	RobotOperator q("name_1", "surname_1", 1000);
	cout << "-----------------print test----------------" << endl;
	q.print();
	cout << "print test was successful."<<endl;
	cout << "---------------------------------------------" << endl;
}
void test3() {
	int x;
	RobotOperator q("name", "surname", 1234);
	cout << "-----------checkAccessCodeTest-------------- " << endl;
	x = q.checkAccessCode(1000);
	cout << "checkAccessCodeTest test was successful." << x << endl;
	cout << "---------------------------------------------" << endl;
}
