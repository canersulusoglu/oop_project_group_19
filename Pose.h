/**
 * @file Pose.h
 * @Author Tarik Kemal Gundogdu (tarikkemal314@gmail.com)
 * @date January, 2020
 * @brief Hold the coordinates of robot.
 *
 * Manage and hold the coordinates of robot.

 */
#ifndef POSE_H
#define POSE_H
#include <cmath>

//! Pose Class
/*!
 Manage and hold the coordinates of robot.
*/
class Pose{

private:
	float x;
	float y;
	float th;

public:
	//! A constructor.
	Pose();
	//! A consttructor.
	Pose(float _x, float _y, float _th);
	//! A destructor.
	~Pose();

	//! Method to get the x coordinate of robot.
	float getX() const; 
	//! Method to get the y coordinate of robot.
	float getY() const;
	//! Method to get the rotation angle of robot.
	float getTh() const; 
	//! Method to get Pose.
	void getPose(float& _x, float& _y, float& _th) const;

	//! Method to set the x coordinate of robot.
	void setX(float _x);
	//! Method to set the y coordinate of robot.
	void setY(float _y);
	//! Method to set the rotation angle of robot.
	void setTh(float _th);
	//! Method to get Pose.
	void setPose(float _x, float _y, float _th);

	//! Method to compare coordinates.
	bool operator== (const Pose& other);
	//! Method to add coordinates.
	Pose operator+ (const Pose& other);
	//! Method to subtract coordinate.
	Pose operator- (const Pose& other);
	//! Method to add to the coordinate.
	Pose& operator+= (const Pose& other);
	//! Method to subtract from the coordinate.
	Pose& operator-= (const Pose& other);
	//! Method to compare coordinates.
	bool operator< (const Pose& other);

	//! Method to find distance between coordinates.
	float findDistanceTo(Pose pos) const;
	//! Method to find angle between coordinates.
	float findAngleTo(Pose pos) const;
};

#endif

