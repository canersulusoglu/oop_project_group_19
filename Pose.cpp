#include "Pose.h"

Pose::Pose()
{
    this->x  = 0;
    this->y  = 0;
    this->th = 0;
}

Pose::Pose(float _x, float _y, float _th)
{
    this->x  = _x;
    this->y  = _y;
    this->th = _th;
}

Pose::~Pose()
{

}

/*!
 \return x coordinate of the position by type float.
*/
float Pose::getX() const
{
    return this->x;
}

/*!
 \return y coordinate of the position by type float.
*/
float Pose::getY() const
{
    return this->y;
}

/*!
 \return rotation angle of the position by type float.
*/
float Pose::getTh() const
{
    return this->th;
}

/*!
 \param _x an int argument.
 \param _y an int argument.
 \param _th an int argument.
*/
void Pose::getPose(float& _x, float& _y, float& _th) const
{
    _x  = this->x;
    _y  = this->y;
    _th = this->th;
}

/*!
 \param _x an int argument..
*/
void Pose::setX(float _x)
{
    this->x = _x;
}

/*!
 \param _y an int argument..
*/
void Pose::setY(float _y)
{
    this->y = _y;
}

/*!
 \param _th an int argument..
*/
void Pose::setTh(float _th)
{
    this->th = _th;
}

/*!
 \param _x an int argument.
 \param _y an int argument.
 \param _th an int argument.
*/
void Pose::setPose(float _x, float _y, float _th)
{
    this->x  = _x;
    this->y  = _y;
    this->th = _th;
}

/*!
 \param other a Pose argument.
 \return true if Poses are equal.
*/
bool Pose::operator==(const Pose& other)
{
    if (this->getX() == other.getX() && this->getY() == other.getY() && this->getTh() == other.getTh())
        return true;
    return false;
}

/*!
 \param other a Pose argument.
 \return addition of two coordinates by type Pose.
*/
Pose Pose::operator+(const Pose& other)
{
    Pose newPose;
    newPose.setX(this->getX() + other.getX());
    newPose.setY(this->getY() + other.getY());

    return newPose;
}

/*!
 \param other a Pose argument.
 \return difference of two coordinates by type Pose.
*/
Pose Pose::operator-(const Pose& other)
{
    Pose newPose;
    newPose.setX(this->getX() - other.getX());
    newPose.setY(this->getY() - other.getY());
  
    return newPose;
}

/*!
 \param other a Pose argument.
 \return Pose.
*/
Pose& Pose::operator-=(const Pose& other)
{
    this->setX(this->getX() - other.getX());
    this->setY(this->getY() - other.getY());

    return *this;
}

/*!
 \param other a Pose argument.
 \return Pose.
*/
Pose& Pose::operator+=(const Pose& other)
{
    this->setX(this->getX() + other.getX());
    this->setY(this->getY() + other.getY());

    return *this;

}

/*!
 \param other a Pose argument.
 \return bool.
*/
bool Pose::operator<(const Pose& other) 
{
    if (this->getX() < other.getX() || (this->getX() == other.getX() && this->getY() < other.getY()))
        return true;
    return false;
}

/*!
 \param other a Pose argument.
 \return distance between two coordinates by type float.
*/
float Pose::findDistanceTo(Pose pos) const
{
    float distance_x = abs(this->getX() - pos.getX());
    float distance_y = abs(this->getY() - pos.getY());
    float distance   = sqrt(pow(distance_x, 2) + pow(distance_y, 2));
    
    return distance;
}

/*!
 \param other a Pose argument.
 \return angle between two coordinates by type float.
*/
float Pose::findAngleTo(Pose pos) const
{
    float dogruEgimi = (this->getY() - pos.getY()) / (this->getX() - pos.getX()); 
    float angleInRadians;
    float angleInDegrees;
    float PI = 3.1415;

    if (dogruEgimi > 0)  
    {
        angleInRadians = atan(dogruEgimi);
        angleInDegrees = angleInRadians * 180 / PI;
    }
    else
    {
        angleInRadians = atan(abs(dogruEgimi));
        angleInDegrees = -1 * (angleInRadians * 180 / PI); 
    }

    return angleInDegrees;
}
