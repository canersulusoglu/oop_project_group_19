/*
 @file Record.h
 @Author Murat Kagan Kayabasi(flowerybeggar@gmail.com)
 @date January, 2020
 @brief Allows navigation in menus.

 Allows navigation in menus.
*/

#ifndef MENU_H
#define MENU_H
#include "RobotControl.h"
using namespace std;

class Menu{
private:
	int selectedMenu;
	bool isOperatorConnected;
	RobotControl* robotControl;
	//! Quit from menu.
	void Quit();
	//! Connect Operator
	void ConnectOperator();
public:
	Menu();
	~Menu();
	//! Main Menu
	void MainMenu();
};

#endif

