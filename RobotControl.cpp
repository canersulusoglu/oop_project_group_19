#include "RobotControl.h"

/*!
 \param name an string argument.
 \param surname an string argument.
 \param code an int argument.
*/
RobotControl::RobotControl(string name, string surname, int code) {
	robotInterface = new PioneerRobotInterface();
	listOfRangeSensors = robotInterface->getRangeSensors();
	robotOperator = new RobotOperator(name, surname, code);
	record = new Record();
	path = new Path();
	isAccessible = false;
}

RobotControl::~RobotControl() {
	delete robotInterface;
	delete[] listOfRangeSensors;
	delete robotOperator;
	delete record;
	delete path;
}


void RobotControl::turnLeft() {
	if (isAccessible) {
		robotInterface->turnLeft();
	}
	else {
		throw std::exception("You are not authorized.");
	}
}


void RobotControl::turnRight() {
	if (isAccessible) {
		robotInterface->turnRight();
	}
	else {
		throw std::exception("You are not authorized.");
	}
}

/*!
 \param speed an float argument.
*/
void RobotControl::forward(float speed) {
	if (isAccessible) {
		robotInterface->forward(speed);
	}
	else {
		throw std::exception("You are not authorized.");
	}
}

/*!
 \param speed an float argument.
*/
void RobotControl::backward(float speed) {
	if (isAccessible) {
		robotInterface->backward(speed);
	}
	else {
		throw std::exception("You are not authorized.");
	}
}


void RobotControl::stopTurn() {
	if (isAccessible) {
		robotInterface->stopTurn();
	}
	else {
		throw std::exception("You are not authorized.");
	}
}


void RobotControl::stopMove() {
	if (isAccessible) {
		robotInterface->stopMove();
	}
	else {
		throw std::exception("You are not authorized.");
	}
}

/*!
 \return The position of the robot by type Pose.
*/
Pose RobotControl::getPose() const{
	if (isAccessible) {
		return robotInterface->getPose();
	}
	else {
		throw std::exception("You are not authorized.");
	}
}

/*!
 \param pose an Pose* argument.
*/
void RobotControl::setPose(Pose* pose) {
	if (isAccessible) {
		robotInterface->setPose(pose);
	}
	else {
		throw std::exception("You are not authorized.");
	}
}


void RobotControl::print() const{
	if (isAccessible) {
		robotOperator->print();
		robotInterface->print();
	}
	else {
		throw std::exception("You are not authorized.");
	}
}

/*!
 \return The state of whether it added location to path.
*/
bool RobotControl::addToPath() {
	if (isAccessible) {
		Pose currentPose = robotInterface->getPose();
		path->addPos(currentPose);
		return true;
	}
	else {
		throw std::exception("You are not authorized.");
	}
}

/*!
 \return The state of whether it deleted all location in path.
*/
bool RobotControl::clearPath() {
	if (isAccessible) {
		delete path;
		path = new Path();
		return true;
	}
	else {
		throw std::exception("You are not authorized.");
	}
}

/*!
 \return The state of whether it records all locations to the file.
*/
bool RobotControl::recordPathToFile() {
	if (isAccessible) {
		string fileName;
		while (true)
		{
			cout << "Filename: " << endl;
			cin >> fileName;
			if (fileName.length() > 0) { break; };
		}
		record->setFileName(fileName);
		bool isFileOpen = record->openFile();
		if (isFileOpen) {
			string location;
			for (int i = 0; i < path->Size(); i++) {
				Pose pose = path->getPos(i);
				location = "x: " + to_string(pose.getX()) + " y: " + to_string(pose.getY()) + " th: " + to_string(pose.getTh());
				record->writeLine(location);
			}
			record->closeFile();
			return true;
		}
		else {
			return false;
		}
	}
	else {
		throw std::exception("You are not authorized.");
	}
}

/*!
 \return The state of whether code is correct.
*/
bool RobotControl::openAccess(int code) {
	bool isCorrectCode = robotOperator->checkAccessCode(code);
	if (isCorrectCode) {
		isAccessible = true;
		return true;
	}
	else {
		isAccessible = false;
		return false;
	}
}

/*!
 \return The state of whether code is correct.
*/
bool RobotControl::closeAccess(int code) {
	bool isCorrectCode = robotOperator->checkAccessCode(code);
	if (isCorrectCode) {
		isAccessible = false;
		return true;
	}
	else {
		isAccessible = true;
		return false;
	}
}