#include "Path.h"

using namespace std;

Path::Path()
{
	this->head = this->tail = nullptr;
	this->number = 0; 
}

Path::~Path()
{
	delete this->tail;
	delete this->head;
}

/*!
 \param pos a Pose argument.
*/
void Path::addPos(Pose pos)  
{
	this->number++; 

	Node* newNode = new Node();
	newNode->pose = pos; 
 
	Node* last = this->head;  

	if (head == nullptr) { this->head = this->tail = newNode; return; }  

	while (last->next != nullptr)
		last = last->next;

	this->tail = last->next = newNode; 
}

void Path::print() const 
{
	if (head == nullptr) { cout << "There is no existing position in the list." << endl; return; }
	
	Node* node = this->head;  

	while (node != nullptr)  
	{ 
		cout << "X coordinate: " << node->pose.getX() << " | " << "Y coordinate: " << node->pose.getY() << " | " << "TH: " << node->pose.getTh() << endl;  // th: değiştirilebilir
		node = node->next;  
	}
	
	cout << endl;	
}

/*!
 \param index an int argument.
*/
Pose& Path::operator[](int index)
{
	try {

		if (index < 0 || index >= this->number)
		{
			throw "[]::Index exceeds the current length of the list.";
		}

		Node* node = this->head;
		unsigned int count = 0; 


		while (node != nullptr)
		{
			if (count == index) { return node->pose; }
			count++;
			node = node->next;
		}

	}
	catch (const char* msg) {
		cerr << msg << endl;
	}
}

/*!
 \param index an int argument.
 \return the position(pose) at the given index by type Pose.
*/
Pose Path::getPos(int index)
{
	try {
		
		if (index < 0 || index >= this->number)
		{
			throw "getPos::Index exceeds the current length of the list.";
		}

		Node* node = this->head;
		unsigned int count = 0; 

		
		while (node != nullptr)
		{
			if (count == index) { return node->pose; }
			count++;
			node = node->next;
		}
	
	} catch (const char* msg) {
		cerr << msg << endl;
	}
}

/*!
 \return size of the list.
*/
int Path::Size()
{
	return this->number;
}

/*!
 \param index an int argument.
 \return true if position(pose) at the given index is removed.
*/
bool Path::removePos(int index)  
{

	if (index < 0 || index >= this->number)
	{
		cout << "Index exceeds the current length of the list." << endl;
		return false;
	}
	else
	{
		Node* node = this->head;
		if (index == 0) 
		{
			this->head = node->next;
			delete node;
			if (head == nullptr) { this->tail = nullptr; }   
		}
		else
		{
			for (int i = 0; i < index - 1; i++)
				node = node->next;

			Node* tmp = node->next; 
			node->next = node->next->next; 
			delete tmp;

			if (index == this->number - 1) { this->tail = node; }
		}
		
		this->number--;

		return true;
	}
}

/*!
 \param index an int argument.
 \return true if argument pose is inserted to the list.
*/
bool Path::insertPos(int index, Pose pose)
{
	if (index < 0 || index > this->number)
	{
		cout << "insertPos::Index exceeds the current length of the list." << endl;
		return false;
	}

	number++;

	Node* node = new Node();
	node->pose = pose;

	if (this->head == nullptr) {
		if (index != 0) {
			return false;
		}
		else { 
			this->head = node;
		}
	}
	
	if (head != nullptr && index == 0) {
		node->next = this->head;
		this->head = node;
		return true;
	}

	Node* current = this->head;
	Node* previous = nullptr;

	int i = 0;

	while (i < index) {
		previous = current;
		current = current->next;

		if (current == nullptr) {
			break;
		}

		i++;
	}

	if (index == number) { this->tail = node; }

	node->next = current;
	previous->next = node;
	
	return true;
}

/*!
 \param p a Pose argument.
*/
void Path::operator>>(const Pose& p)
{
	this->addPos(p);
}



