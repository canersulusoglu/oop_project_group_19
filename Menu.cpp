#include "Menu.h"

Menu::Menu() {
	robotControl = NULL;
	isOperatorConnected = false;
	selectedMenu = NULL;
}

Menu::~Menu() {
	if (robotControl != NULL)
		delete robotControl;
}

void Menu::Quit() {
	exit(0);
}

void Menu::ConnectOperator() {
	if (!isOperatorConnected) {
		cout << "---- Operator Connection ----" << endl;
		string name, surname;
		int accessCode;
		while (true) {
			cout << "Name: ";
			cin >> name;
			cout << "Surname: ";
			cin >> surname;
			cout << "Access Code: ";
			cin >> accessCode;

			if (accessCode >= 1000 && name.length() > 0 && surname.length() > 0) {
				isOperatorConnected = true;
				robotControl = new RobotControl(name, surname, accessCode);
				break;
			}
		}
	}
}

void Menu::MainMenu() {
	if (!isOperatorConnected) {
		ConnectOperator();
	}
	cout << "------------------------------" << endl;
	cout << "Menu" << endl;
	cout << "1. Move Robot Forward" << endl;
	cout << "2. Move Robot Backward" << endl;
	cout << "3. Turn Robot Right" << endl;
	cout << "4. Turn Robot Left" << endl;
	cout << "5. Stop Robot Moving" << endl;
	cout << "6. Stop Robot Turning" << endl;
	cout << "7. Add Current Pose To Path" << endl;
	cout << "8. Clear All Path" << endl;
	cout << "9. Record Path to File" << endl;
	cout << "10. Open Access" << endl;
	cout << "11. Close Access" << endl;
	cout << "12. Print Infos" << endl;
	cout << "13. Quit" << endl;
	cout << "Choose one: ";
	cin >> selectedMenu;

	int code;
	switch (selectedMenu) {
	case 1:
		system("cls");
		cout << "<Motion - Forward>" << endl;
		try{
			robotControl->forward(1000);
		}
		catch (const std::exception& e){
			cerr << e.what() << endl;
		}
		MainMenu();
		break;
	case 2:
		system("cls");
		cout << "<Motion - Backward>" << endl;
		try {
			robotControl->backward(1000);
		}
		catch (const std::exception& e) {
			cerr << e.what() << endl;
		}
		MainMenu();
		break;
	case 3:
		system("cls");
		cout << "<Motion - Turn Right>" << endl;
		try {
			robotControl->turnRight();
		}
		catch (const std::exception& e) {
			cerr << e.what() << endl;
		}
		MainMenu();
		break;
	case 4:
		system("cls");
		cout << "<Motion - Turn Left>" << endl;
		try {
			robotControl->turnLeft();
		}
		catch (const std::exception& e) {
			cerr << e.what() << endl;
		}
		MainMenu();
		break;
	case 5:
		system("cls");
		cout << "<Motion - Stop Move>" << endl;
		try {
			robotControl->stopMove();
		}
		catch (const std::exception& e) {
			cerr << e.what() << endl;
		}
		MainMenu();
		break;
	case 6:
		system("cls");
		cout << "<Motion - Stop Turn>" << endl;
		try {
			robotControl->stopTurn();
		}
		catch (const std::exception& e) {
			cerr << e.what() << endl;
		}
		MainMenu();
		break;
	case 7:
		system("cls");
		cout << "<Motion - Add Current Pose to Path>" << endl;
		try {
			bool isAdded = robotControl->addToPath();
			if (isAdded) {
				cout << "Successfully added current pose to path." << endl;
			}
			else {
				cerr << "An error occurred while adding the current pose to path." << endl;
			}
		}
		catch (const std::exception& e) {
			cerr << e.what() << endl;
		}
		MainMenu();
		break;
	case 8:
		system("cls");
		cout << "<Motion - Clear All Path>" << endl;
		try {
			bool isCleaned = robotControl->clearPath();
			if (isCleaned) {
				cout << "Successfully cleaned path to file." << endl;
			}
			else {
				cerr << "An error occurred while clear the path." << endl;
			}
		}
		catch (const std::exception& e) {
			cerr << e.what() << endl;
		}
		MainMenu();
		break;
	case 9:
		system("cls");
		cout << "<Motion - Record Path to File>" << endl;
		try {
			bool isRecorded = robotControl->recordPathToFile();
			if (isRecorded) {
				cout << "Successfully recorded path to file." << endl;
			}
			else {
				cerr << "An error occurred while saving to file." << endl;
			}
		}
		catch (const std::exception& e) {
			cerr << e.what() << endl;
		}
		MainMenu();
		break;
	case 10:
		system("cls");
		cout << "<Motion - Open Access>" << endl;
		try {
			cout << "Enter code: ";
			cin >> code;
			bool isCorrectCode = robotControl->openAccess(code);
			if (isCorrectCode) {
				cout << "Code is correct.You can access to all commands now." << endl;
			}
			else {
				cerr << "Code is incorrect." << endl;
			}
		}
		catch (const std::exception& e) {
			cerr << e.what() << endl;
		}
		MainMenu();
		break;
	case 11:
		system("cls");
		cout << "<Motion - Close Access>" << endl;
		try {
			cout << "Enter code: ";
			cin >> code;
			bool isCorrectCode = robotControl->closeAccess(code);
			if (isCorrectCode) {
				cout << "Code is correct.Successfully access is closed to all commands." << endl;
			}
			else {
				cerr << "Code is incorrect." << endl;
			}
		}
		catch (const std::exception& e) {
			cerr << e.what() << endl;
		}
		MainMenu();
		break;
	case 12:
		system("cls");
		cout << "<Print - Informations>" << endl;
		try {
			robotControl->print();
		}
		catch (const std::exception& e) {
			cerr << e.what() << endl;
		}
		MainMenu();
		break;
	case 13:
		system("cls");
		Quit();
		break;
	default:
		system("cls");
		MainMenu();
		break;
	}
}