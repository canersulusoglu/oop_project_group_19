/**
 * @file LaserSensorTest.cpp
 * @Author Caner Sulusoglu (cscaner26@gmail.com)
 * @date January, 2020
 * @brief LaserSensor class test file.
 *
 * LaserSensor class test file.
 */

#include <iostream>
#include "LaserSensor.h"
using namespace std;

PioneerRobotAPI* robot;
LaserSensor* laserSensor;

void Test1();
void Test2();
void Test3();
void Test4();
void Test5();

int main() {
	robot = new PioneerRobotAPI();

	if (!robot->connect()) {
		cout << "Could not connect..." << endl;
		return 0;
	}

	try
	{
		laserSensor = new LaserSensor(robot);

		Test1();
		Test2();
		Test3();
		Test4();
		Test5();
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << endl;
	}

	robot->disconnect();
	delete laserSensor;
	delete robot;
	return 0;
}

void Test1() {
	float* laserData = new float[181];
	laserSensor->updateSensor(laserData);
	cout << "----  Get Lasers Data Test   ----" << endl;
	for (int i = 0; i < 181; i++) {
		cout << "Laser Index: " << i << " Range: " << laserData[i] << " Angle: " << laserSensor->getAngle(i) << endl;
	}
	cout << "--------------------------------" << endl;
}

void Test2() {
	cout << "----  Max Laser Data Test   ----" << endl;
	int MaxLaserIndex = -1;
	float MaxLaserRange = laserSensor->getMax(MaxLaserIndex);
	cout << "Maximum Laser Index: " << MaxLaserIndex << " Range: " << MaxLaserRange << " Angle: " << laserSensor->getAngle(MaxLaserIndex) << endl;
	cout << "--------------------------------" << endl;
}

void Test3() {
	cout << "----  Min Laser Data Test   ----" << endl;
	int MinLaserIndex = -1;
	float MinLaserRange = laserSensor->getMin(MinLaserIndex);
	cout << "Minimum Laser Index: " << MinLaserIndex << " Range: " << MinLaserRange << " Angle: " << laserSensor->getAngle(MinLaserIndex) << endl;
	cout << "--------------------------------" << endl;
}

void Test4() {
	cout << "----  Closest Range Test   ----" << endl;
	float ClosestRangeAngle1;
	float ClosestRange1 = laserSensor->getClosestRange(0, 90, ClosestRangeAngle1);
	cout << "Closest Laser Range between 0 and 90 angles: " << ClosestRange1 << " Angle: " << ClosestRangeAngle1 << endl;

	float ClosestRangeAngle2;
	float ClosestRange2 = laserSensor->getClosestRange(90, 180, ClosestRangeAngle2);
	cout << "Closest Laser Range between 90 and 180 angles: " << ClosestRange2 << " Angle: " << ClosestRangeAngle2 << endl;

	float ClosestRangeAngle3;
	float ClosestRange3 = laserSensor->getClosestRange(-45, 45, ClosestRangeAngle3);
	cout << "Closest Laser Range between -45 and 45 angles: " << ClosestRange3 << " Angle: " << ClosestRangeAngle3 << endl;

	float ClosestRangeAngle4;
	float ClosestRange4 = laserSensor->getClosestRange(-60, 60, ClosestRangeAngle4);
	cout << "Closest Laser Range between -60 and 60 angles: " << ClosestRange4 << " Angle: " << ClosestRangeAngle4 << endl;
	cout << "--------------------------------" << endl;
}

void Test5() {
	cout << "----  Non Exist Laser Index Test  ----" << endl;
	cout << "Sonar Index: 200 " << " Range: " << (*laserSensor)[200] << endl;
	cout << "--------------------------------" << endl;
}