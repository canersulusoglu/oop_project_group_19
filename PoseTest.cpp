/**
 * @file Pose.h
 * @Author Tarik Kemal Gundogdu (tarikkemal314@gmail.com)
 * @date January, 2020
 * @brief Pose class test file.
 *
 * Pose class test file.

 */

#include "Pose.h"
#include <iostream>

using namespace std;

void test1(Pose& pose);
void test2(Pose& pose, Pose& pose2);
void test3(Pose& pose, Pose& pose2);


int main()
{
	try
	{
		Pose pose(1, 2, 3);
		Pose pose2(7, 8, 9);

		test1(pose);
		test2(pose, pose2);
		test3(pose, pose2);
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << endl;
	}

}

void test1(Pose& pose)
{
	cout << "---Set and get functions test---" << endl << endl;
	pose.setX(2);
	pose.setY(3);
	pose.setTh(4);
	cout << "X coordinate: " << pose.getX() << " | " << "Y coordinate: " << pose.getY() << " | " << "TH: " << pose.getTh() << endl;
	cout << "-----------------------------" << endl << endl;
}

void test2(Pose& pose, Pose& pose2)
{
	cout << "---Find angle and distance functions test---" << endl << endl;
	cout << "pose's angle to pose2: "    << pose.findAngleTo(pose2)    << endl;
	cout << "pose's distance to pose2: " << pose.findDistanceTo(pose2) << endl;
	cout << "-----------------------------" << endl << endl;
}

void test3(Pose& pose, Pose& pose2)
{
	cout << "---Operators test---" << endl << endl;
	Pose tmp;
	tmp = pose + pose2;
	cout << "pose + pose2 = " << "(" << tmp.getX() << ", " << tmp.getY() << ", " << tmp.getTh() << ")" << endl;
	tmp = pose - pose2;
	cout << "pose - pose2 = " << "(" << tmp.getX() << ", " << tmp.getY() << ", " << tmp.getTh() << ")" << endl;
	if (pose == pose2) 
		cout << "pose is equal to pos2" << endl;
	else if (pose < pose2) 
		cout << "pose is smaller than pos2" << endl;
	else 
		cout << "pose is bigger than pos2" << endl;

	cout << "-----------------------------" << endl << endl;

}

