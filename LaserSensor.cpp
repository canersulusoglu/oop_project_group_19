#include "LaserSensor.h"

LaserSensor::~LaserSensor() {
	delete ranges;
}

/*!
 \param index an integer argument.
 \return The range of sensor by index.
*/
float LaserSensor::getRange(int index) {
	if (index >= 0 && index <= 180) {
		return ranges[index];
	}
	else {
		throw std::out_of_range("Out of range.");
	}
}

/*!
 \param index an integer argument.
 \return The angle of sensor by index.
*/
float LaserSensor::getAngle(int index) {
	if (index >= 0 && index <= 180) {
		float robotAngle = robotAPI->getTh(); // For Relative Angle. Without that it will be absolute angle.
		return 90 - index + robotAngle;
	}
	else {
		throw std::out_of_range("Out of range.");
	}
}

/*!
 \param index an reference integer argument.
 \return The maximum range.
*/
float LaserSensor::getMax(int& index) {
	int MaxIndex = 0;
	float Max = ranges[MaxIndex];
	for (int i = 1; i < 181; i++) {
		if (ranges[i] > Max) {
			Max = ranges[i];
			MaxIndex = i;
		}
	}
	index = MaxIndex;
	return Max;
}

/*!
 \param index an reference integer argument.
 \return The minimum range.
*/
float LaserSensor::getMin(int& index) {
	int MinIndex = 0;
	float Min = ranges[MinIndex];
	for (int i = 1; i < 181; i++) {
		if (ranges[i] < Min) {
			Min = ranges[i];
			MinIndex = i;
		}
	}
	index = MinIndex;
	return Min;
}

/*!
 \param startAngle an float argument.
 \param endAngle an float argument.
 \param angle an reference float argument.
 \return The range of the smallest distance between startAngle and endAngle angles.
*/
float LaserSensor::getClosestRange(float startAngle, float endAngle, float& angle) {
	float ClosestRange = ranges[0];
	float ClosestAngle = getAngle(0);

	float laserAngle;
	for (int i = 1; i < 181; i++) {
		laserAngle = getAngle(i);
		if (startAngle < laserAngle && endAngle > laserAngle) {
			if (ranges[i] < ClosestRange) {
				ClosestAngle = laserAngle;
				ClosestRange = ranges[i];
			}
		}
	}
	angle = ClosestAngle;
	return ClosestRange;
}

/*!
 \param ranges[181] an float array argument.
*/
void LaserSensor::updateSensor(float ranges[181]) {
	robotAPI->getLaserRange(this->ranges);
	if (ranges != NULL)
		ranges = this->ranges;
}