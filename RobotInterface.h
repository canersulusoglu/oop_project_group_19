/**
 * @file RobotInterface.h
 * @Author Metin Cem Demirdas (cem.dmrds@gmail.com)
 * @date January, 2021
 * @brief an operator class for RobotControl class to control.
 *
 * @brief an abstract class for interfaces.
 */
#ifndef ROBOTINTERFACE_H
#define ROBOTINTERFACE_H
#include<iostream>
#include<list>
#include"Pose.h"
#include"RangeSensor.h"
using namespace std;
//! RobotInterface class.
/*!
 an abstract class whose inherited member/s have direct access to PioneerRobotAPI class.
*/
class RobotInterface {
protected:
	bool isRobotConnected;/*!< this variable controls if the robot is connected  */
	Pose* position;/*!< used to initialize pose */
	list<RangeSensor*>* rangeSensors;/*!< all sensors will be kept in this list  */

public:
	//! Constructor of RobotInterface class. 
	RobotInterface() { position = new Pose(0, 0, 0); isRobotConnected=false; rangeSensors = new list<RangeSensor*>(); }
	//! Destructor of Robot	Interface class. 
	virtual ~RobotInterface() { delete position; }
	//! turns robot to the left, then fixes the position of the robot .
	virtual void turnLeft() = 0;
	//! turns robot to the right, then fixes the position of the robot .
	virtual void turnRight() = 0;
	//! moves the robot forwards, then fixes the position of the robot .
	virtual void forward(float speed) = 0;
	//! prints current position of the robot.
	virtual void print() const = 0;
	//! moves the robot backwards, then fixes the position of the robot .
	virtual void backward(float speed) = 0;
	//! stops turning the robot, then fixes the position of the robot .
	virtual void stopTurn() = 0;
	//! stops moving the robot, then fixes the position of the robot .
	virtual void stopMove() = 0;
	//!Gets the position of the robot.
	virtual Pose getPose() const = 0;
	//!Gets the position of the robot.
	virtual void setPose(Pose* pose) = 0;
	//! updates each sensor
	virtual void updateSensors() = 0;
	//! Gets all the sensors.
	virtual list<RangeSensor*>* getRangeSensors() const=0;
};
#endif