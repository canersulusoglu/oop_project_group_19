/**
 * @file Encryption.h
 * @Author Metin Cem Demirdas (cem.dmrds@gmail.com)
 * @date January, 2021
 * @brief gets a number and encrypts/decrypts it.
 *
 * @brief gets a 4 digit number and encrypts/decrypts it with given algorithm.
 */
#ifndef ENCRYPTION_H
#define ENCRYPTION_H
#include<iostream>
#include<math.h>
using namespace std;

//! Encryption class.
/*!
 gets a number and encrypts/decrypts it.
*/
class Encryption {
public:

	//! This function encrypts a given 4 digit number, based on given algorithm. 
	int encrypt(int);
	
	//! This function decrypts a given 4 digit number, the reverse process of encrypt function. 
	int decrypt(int number);
};
#endif