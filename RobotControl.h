/**
 * @file RobotControl.h
 * @Author Caner Sulusoglu (cscaner26@gmail.com)
 * @date January, 2020
 * @brief Access information about robot.
 *
 * Access information about robot movement and position of the robot provides.
 */
#ifndef ROBOTCONTROL_H
#define ROBOTCONTROL_H
#include "RobotInterface.h"
#include "PioneerRobotInterface.h"
#include "RangeSensor.h"
#include "RobotOperator.h"
#include "Record.h"
#include "Path.h"
#include <stdio.h>
#include <list>
using namespace std;

//! RobotControl Class
/*!
 Access information about robot movement and position of the robot provides.
*/
class RobotControl{
private:
	RobotInterface* robotInterface;
	list<RangeSensor*>* listOfRangeSensors;
	RobotOperator* robotOperator;
	Record* record;
	Path* path;
	bool isAccessible;
public:
	//! A constructor.
	RobotControl(string name, string surname, int code);
	//! A destructor. 
	~RobotControl();
	//! Method that turns the robot left. 
	void turnLeft();
	//! Method that turns the robot right.
	void turnRight();
	//! Method that moves the robot forward.
	void forward(float speed);
	//! Method that moves the robot backward.
	void backward(float speed);
	//! Method that stops turning the robot.
	void stopTurn();
	//! Method that stops moving the robot.
	void stopMove();
	//! Method that returns position of the robot.
	Pose getPose() const;
	//! Method that sets position of the robot.
	void setPose(Pose* pose);
	//! Method that prints informations of the robot.
	void print() const;
	//! Method that add to path the current location of the robot.
	bool addToPath();
	//! Method that clear all path of robot.
	bool clearPath();
	//! Method that writes the locations inside the path object to the file.
	bool recordPathToFile();
	//! Method that gives permission to access commands.
	bool openAccess(int code);
	//! Method that denies access to commands.
	bool closeAccess(int code);
};
#endif