/**
 * @file Path.h
 * @Author Tarik Kemal Gundogdu (tarikkemal314@gmail.com)
 * @date January, 2020
 * @brief Manage road plan.
 *
 * Manage and hold road plan in linked list form.

 */
#ifndef PATH_H
#define PATH_H
#include "Node.h"
#include "Pose.h"
#include <iostream>

using namespace std;

//! Path Class
/*!
 Manage and hold road plan in linked list form.
*/
class Path {
private:
	Node* tail;
	Node* head;
	int number;  
public:
	//! A constructor.
	Path();
	//! A destructor. 
	~Path();
	//! Method that inserts pose to end of the list.
	void addPos(Pose pos);
	//! Method that prints all the positions(pose) in the list.
	void print() const;
	//! Method that returns the position(pose) given at the index.
	Pose& operator[](int index);
	//! Method that returns the position(pose) given at the index.
	Pose getPos(int index);
	//! Method that returns the size of the list.
	int Size();
	//! Method that removes the position(pose) at the given index.
	bool removePos(int index);
	//! Method that inserts position(pose) at the given index.
	bool insertPos(int index, Pose pose);
	//! Method that inserts the given pose at the end of the list.
	void operator>>(const Pose& p);
	//! Method that prints all the positions(pose) in the list.
	friend ostream& operator<<(ostream& os, const Path& p) 
	{
		if (p.head == nullptr) { os << "There is no existing position in the list." << endl; return os; }
		Node* node = p.head;  
		while (node != nullptr) 
		{
			os << "X coordinate: " << node->pose.getX() << " | " << "Y coordinate: " << node->pose.getY() << " | " << "TH: " << node->pose.getTh() << endl;  // th: değiştirilebilir
			node = node->next;  
		}
		os << endl;
		return os;
	}
};

#endif



