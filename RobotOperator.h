/**
 * @file RobotOperator.h
 * @Author Metin Cem Demirdas (cem.dmrds@gmail.com)
 * @date January, 2021
 * @brief saves the operator's info and checks the accesscode.
 *
 * saves the operator's info(also the accesscode) and checks the accesscode by encrypting and decrypting.
 */

#ifndef ROBOTOPERATOR_H
#define ROBOTOPERATOR_H
#include<iostream>
#include<string>
#include"Encryption.h"
using namespace std;

//! RobotOperator class.
/*!
 saves the operator's info(also the accesscode) and checks the accesscode by encrypting and decrypting.
*/
class RobotOperator {
private:
	Encryption encryption; /*!< encryption object, it's used in encrypt/decrypt functions */
	string name; /*!< operator's name */
	string surname; /*!< operator's surname */
	unsigned int accessCode; /*!< it's used to check the access code */
	bool accessState; /*!< checks the access state */
	//! This function encrypts a given 4 digit number(by using the Encryption class). 
	int encryptCode(int);
	//! This function decrypts a given 4 digit number(by using the Encryption class). 
	int decryptCode(int);
public:
	
	//! Constructor of RobotOperator class. 
	RobotOperator(string, string, int);

	//! This function compares the access code and the given code, if they are equal,returns true. 
	bool checkAccessCode(int number);

	//! This function prints the operator name and operation status.
	void print();
};
#endif