#include"Encryption.h";
#include<math.h>
using namespace std;

/*!
 \param number an integer argument.
 \return The encrypted number
*/
int Encryption::encrypt(int number) {
	if (number >= 1000 && number < 10000)
	{
		int remainder, new_number, digit_count = 0, encrypted = 0;
		while (number > 0)
		{
			remainder = number % 10;
			new_number = (remainder + 7) % 10;
			digit_count++;
			encrypted += new_number * pow(10, (digit_count - 1));
			number = number / 10;
		}
		int* digits;
		digits = new int[digit_count];
		int encrypted_copy = encrypted;

		for (int i = 0; i < digit_count; i++)
		{
			digits[i] = encrypted_copy % 10;
			encrypted_copy = encrypted_copy / 10;
			if (i >= 2)
			{
				int buffer = digits[i];
				digits[i] = digits[i - 2];
				digits[i - 2] = buffer;
			}
		}

		encrypted = 0;

		for (int i = 0; i < digit_count; i++)
		{
			encrypted += digits[i] * pow(10, i);
		}
		delete[] digits;
		cout << "encryption is successful. Encrypted number is : " << encrypted << endl;
		return encrypted;
	}
	else {
		cout << "Error! given code must be 4 digits. ";
		return 0;
	}

};

/*!
 \param number an integer argument.
 \return The decrypted number
*/
int Encryption::decrypt(int number) {

	int digit_count = 0, remainder, a_copy = number, buffer, decrypted = 0;

	while (a_copy > 0)
	{
		a_copy = a_copy / 10;
		digit_count++;
	}
	if (digit_count < 4)
	{
		digit_count = 4;
	}
	int* digits;
	digits = new int[digit_count];

	for (int i = 0; i < digit_count; i++)
	{
		remainder = number % 10;
		digits[i] = remainder;
		number = number / 10;
		if (i > 1)
		{
			buffer = digits[i];
			digits[i] = digits[i - 2];
			digits[i - 2] = buffer;
		}
	}

	for (int i = 0; i < digit_count; i++)
	{
		if (digits[i] - 7 < 0)
		{
			digits[i] = digits[i] + 10 - 7;
		}
		else {
			digits[i] = digits[i] - 7;
		}
		decrypted += digits[i] * pow(10, i);
	}
	delete[] digits;
	cout << "decryption is successful. Encrypted number is : " << decrypted << endl;
	return decrypted;
};

