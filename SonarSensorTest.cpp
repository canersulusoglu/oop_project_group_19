/**
 * @file SonarSensorTest.cpp
 * @Author Caner Sulusoglu (cscaner26@gmail.com)
 * @date January, 2020
 * @brief SonarSensor class test file.
 *
 * SonarSensor class test file.
 */

#include <iostream>
#include "SonarSensor.h"
using namespace std;

PioneerRobotAPI* robot;
SonarSensor* sonarSensor;

void Test1();
void Test2();
void Test3();
void Test4();

int main() {
	robot = new PioneerRobotAPI();
	if (!robot->connect()) {
		cout << "Could not connect..." << endl;
		return 0;
	}

	try
	{
		sonarSensor = new SonarSensor(robot);

		Test1();
		Test2();
		Test3();
		Test4();

	}
	catch (const std::exception& e)
	{
		cerr << e.what() << endl;
	}

	robot->disconnect();
	delete sonarSensor;
	delete robot;
	return 0;
}

void Test1() {
	float sonarsData[16];
	sonarSensor->updateSensor(sonarsData);
	cout << "----  Get Sonars Data Test   ----" << endl;
	for (int i = 0; i < 16; i++) {
		cout << "Sonar Index: " << i << " Range: " << sonarsData[i] << " Angle: " << sonarSensor->getAngle(i) << endl;
	}
	cout << "--------------------------------" << endl;
}

void Test2() {
	cout << "----  Max Sonar Data Test   ----" << endl;
	int MaxSensorIndex = -1;
	float MaxSonarRange = sonarSensor->getMax(MaxSensorIndex);
	cout << "Maximum Sonar Index: " << MaxSensorIndex << " Range: " << MaxSonarRange << " Angle: " << sonarSensor->getAngle(MaxSensorIndex) << endl;
	cout << "--------------------------------" << endl;
}

void Test3() {
	cout << "----  Min Sonar Data Test   ----" << endl;
	int MinSensorIndex = -1;
	float MinSonarRange = sonarSensor->getMin(MinSensorIndex);
	cout << "Minimum Sonar Index: " << MinSensorIndex << " Range: " << MinSonarRange << " Angle: " << sonarSensor->getAngle(MinSensorIndex) << endl;
	cout << "--------------------------------" << endl;
}

void Test4() {
	cout << "----  Non Exist Sonar Index Test  ----" << endl;
	cout << "Sonar Index: 18 " << " Range: " << (*sonarSensor)[18] << endl;
	cout << "--------------------------------" << endl;
}