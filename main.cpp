#include "Menu.h"
#include <iostream>
using namespace std;

int main() {	
	Menu* menu = new Menu();
	try
	{
		menu->MainMenu();
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << endl;
	}

	delete menu;
	return 0;
}