#include"RobotOperator.h"
using namespace std;

/*!
 \param number an integer argument.
 \return The encrypted number
*/
int RobotOperator::encryptCode(int number) {
	int encrypted;
	encrypted = encryption.encrypt(number);
	return encrypted;
}

/*!
 \param number an integer argument.
 \return The decrypted number
*/
int RobotOperator::decryptCode(int number) {
	int decrypted;
	decrypted = encryption.decrypt(number);
	return decrypted;
}
/*!
 \param name a string argument.
 \param surname a string argument.
 \param number an integer argument.
*/
RobotOperator::RobotOperator(string name, string surname, int number) {
	this->name = name;
	this->surname = surname;
	this->accessCode = encryptCode(number);
	this->accessState = false;
}
/*!
 \param number an integer argument.
 \return The access state
*/
bool RobotOperator::checkAccessCode(int number) {
	int x = decryptCode(this->accessCode);
	if (number == x) { 
		cout << "accesscode is valid." << number << "=" << x<<endl;
		this->accessState = true; }
	else { 
		cout << "accesscode is not valid." << number << "!=" << x<<endl;
		this->accessState = false; }
	return this->accessState;
}

void RobotOperator::print() {
	cout << "name of the operator : " << this->name << endl;
	cout << "surname of the operator : " << this->surname << endl;
	cout << "access status is : " << this->accessState << endl;
}

