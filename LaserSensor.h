/**
 * @file LaserSensor.h
 * @Author Caner Sulusoglu (cscaner26@gmail.com)
 * @date January, 2020
 * @brief Provides data retention and management for laser distance sensor.
 *
 * Provides data retention and management for laser distance sensor.
 */
#ifndef LASERSENSOR_H
#define LASERSENSOR_H
#include "RangeSensor.h"

//! LaserSensor Class
/*!
  Provides data retention and management for laser distance sensor.
*/
class LaserSensor : public RangeSensor{
public:
	//! A constructor.
	LaserSensor(PioneerRobotAPI* robot) :RangeSensor(robot) { this->ranges = new float[181]; };
	//! A destructor.
	~LaserSensor();
	//! Method that returns range of sensor by their index.
	float getRange(int index);
	//! Method that returns angle of sensor by their index.
	float getAngle(int index);
	//! Method that returns sensor that has maximum range and store its index to parameter.
	float getMax(int& index);
	//! Method that returns sensor that has minimum range and store its index to parameter.
	float getMin(int& index);
	//! Method that returns the range of the smallest distance between startAngle and endAngle angles and store its angle to parameter.
	float getClosestRange(float startAngle, float endAngle, float& angle);
	//! Method that store all range of sensors to ranges 181 capacity array.
	void updateSensor(float ranges[181]);
};
#endif