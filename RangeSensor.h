/**
 * @file RangeSensor.h
 * @Author Tarik Kemal Gundogdu (tarikkemal314@gmail.com)
 * @date January, 2020
 * @brief Abstract class.
 *
 * Abstract class for sensor classes.

 */
#ifndef RANGESENSOR_H
#define RANGESENSOR_H
#include "PioneerRobotAPI.h"

 //! RangeSensor Class
 /*!
  Serve as an interface for sensor classes.
 */
class RangeSensor {
protected:
	float* ranges;
	PioneerRobotAPI* robotAPI;
public:
	//! A constructor.
	RangeSensor(PioneerRobotAPI* robot);
	//! A destructor.
	virtual ~RangeSensor();
	//! Method that returns range of sensor by their index.
	virtual float getRange(int index) = 0;
	//! Method that returns angle of sensor by their index.
	virtual float getAngle(int index) = 0;
	//! Method that returns sensor that has maximum range and store its index to parameter.
	virtual float getMax(int&  index) = 0;
	//! Method that returns sensor that has minimum range and store its index to parameter.
	virtual float getMin(int&  index) = 0;
	//! Operator that returns range of sensor by their index.
	float operator[](int i);
	//! Method that store all range of sensors to ranges 16 capacity array.
	virtual void  updateSensor(float ranges[]) = 0;
	//! Method that returns the range of the smallest distance between startAngle and endAngle angles and store its angle to parameter.
	virtual float getClosestRange(float startAngle, float endAngle, float& angle) = 0;	
};

#endif
