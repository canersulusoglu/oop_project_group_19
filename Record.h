/**
* @file Record.h
* @Author Murat Kagan Kayabasi(flowerybeggar@gmail.com)
* @date January, 2020
* @brief It saves to file.
*
* It saves to file.
*/
#ifndef RECORD_H
#define RECORD_H

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class Record {
private:
	string fileName;
	fstream file;
public:
	Record();
	Record(string file_name);
	bool openFile();
	bool closeFile();
	bool writeLine(string str);
	void setFileName(string name);
	string readLine();
	friend ostream& operator << (ostream&, const Record&);
	friend istream& operator >> (istream&, Record&);
};
#endif